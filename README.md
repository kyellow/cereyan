# cereyan
Systems Analysis and Design Final Project (Spring 2012)

## Description
cereyan is specialized ERP(Enterprise Resource Planning) system that was implemented for the company providing services in the field of electrical installations.

## Relational Database schema
<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/database.png" width="400" height="200"/>

## ScreenShot

 * Odeme Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/odeme_panel.png" width="300" height="200"/>

 * Fatura Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/fatura_panel.png" width="300" height="200"/>

 * Müşteri Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/musteri_panel.png" width="300" height="200"/>

 * Personel Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/personel_panel.png" width="300" height="200"/>

 * Stok Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/stok_panel.png" width="300" height="200"/>


Sub Panels : 
	<a href="screenshot/README.md">screenshot/README.md</a>
