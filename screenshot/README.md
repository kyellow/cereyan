
## ScreenShot

 * Odeme Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/odeme_panel.png" width="300" height="200"/>
<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/tema_metal.png" width="300" height="200"/>
<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/tema_system.png" width="300" height="200"/>

 * Fatura Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/fatura_panel.png" width="300" height="200"/>

 * Fatura Add Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/fatura_add.png" width="300" height="200"/>

 * Müşteri Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/musteri_panel.png" width="300" height="200"/>

 * Müşteri Add Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/musteri_add.png" width="300" height="200"/>

 * Müşteri Update Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/musteri_update.png" width="300" height="200"/>

 * Personel Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/personel_panel.png" width="300" height="200"/>

 * Personel Add Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/personel_add.png" width="300" height="200"/>

 * Personel Update Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/personel_update.png" width="300" height="200"/>

 * Stok Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/stok_panel.png" width="300" height="200"/>

 * Urun Add Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/urun_add_1.png" width="300" height="200"/>
<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/urun_add_2.png" width="300" height="200"/>

 * Urun Update Panel

<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/urun_update_1.png" width="300" height="200"/>
<img src="https://gitlab.com/kyellow/cereyan/raw/master/screenshot/urun_update_2.png" width="300" height="200"/>
