/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author kyellow
 */
public class Odeme {

    public static final Object[] ODEME_COLUMN = {"Müşteri Ad-Soyad", "Fatura No", "Toplam Borç", "Müşteri Telefon", "Müşteri Adres"};

    private String ad_soyad;
    private int fatura_id;
    private String tel;
    private String adres;
    private float borc;

    public Odeme(String as, int fid, String tel, String adres, float borc) {
        this.ad_soyad = as;
        this.fatura_id = fid;
        this.tel = tel;
        this.adres = adres;
        this.borc = borc;
    }

    public static Object[][] getAllOdeme(String s) {
        Object[][] odemeList = null;
        String sorgu = "FROM Siparis AS S INNER JOIN Musteri AS M USING(musteri_id) WHERE (Kalan)!=0 AND (M.ad_soyad LIKE '" + s + "%' OR M.musteri_id LIKE '" + s + "%');";
        try {

            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount " + sorgu);

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                odemeList = new Object[rowIndex][ODEME_COLUMN.length];
                rs = pstmt.executeQuery("SELECT ad_soyad,fatura_id,cep_tel,adres, kalan as borc " + sorgu);
                rowIndex = 0;

                while (rs.next()) {
                    odemeList[rowIndex][0] = rs.getString("ad_soyad");
                    odemeList[rowIndex][1] = rs.getInt("fatura_id");
                    odemeList[rowIndex][2] = rs.getFloat("borc");
                    odemeList[rowIndex][3] = rs.getString("cep_tel");
                    odemeList[rowIndex][4] = rs.getString("adres");
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tüm ödemeleri elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }

        return odemeList;
    }
}
