/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author kyellow
 */
public class Tools {    
    
    public enum MessageType
    {
        ERROR, WARNING, INFORMATION
    }
    
    public static Timestamp newStamp() {
        return new Timestamp((new java.util.Date()).getTime());
    }

    public static String showDate(Timestamp ts) {
        Date date = new Date(ts.getTime());
        String gosterim = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(gosterim);
        return sdf.format(date);
    }

    public static void showMsg(String mesaj, MessageType mesajTipi) {
        
        switch(mesajTipi)
        {
            case WARNING :
                JOptionPane.showMessageDialog(new JFrame(), mesaj, "Uyarı", JOptionPane.WARNING_MESSAGE);
                break;
            case ERROR :
                JOptionPane.showMessageDialog(new JFrame(), mesaj, "Hata", JOptionPane.ERROR_MESSAGE);
                break;
            case INFORMATION :
                JOptionPane.showMessageDialog(new JFrame(), mesaj, "Bilgi", JOptionPane.INFORMATION_MESSAGE);
                break;
        }   
    }

    public static void logMsg(String mesaj,MessageType type)
    {
        switch(type)
        {
            case WARNING :
                break;
            case ERROR :
                System.out.println(mesaj);
                break;
            case INFORMATION :
                break;
        }
    }
    
    public static int showDialog(String soruIcerik) {
        Object[] secenekler = {"EVET", "HAYIR"};
        int n = JOptionPane.showOptionDialog(new JFrame(), soruIcerik, "?", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, secenekler, secenekler[1]);
        return n;
    }

}
