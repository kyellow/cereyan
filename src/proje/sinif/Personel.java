/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author kyellow
 */
public class Personel {

    public static final Object[] PERSONEL_COLUMN = {"Kimlik No", "Ad-Soyad", "Adres", "Telefon", "Pozisyon", "Başlangıç Tarihi"," Maas"};
    public static final Object[] FATURALIPERSONEL_COLUMN = { "Personel Ad-Soyad", "Ücret", "Açıklama"};
    public static final Object[] SIPARISPERSONEL_COLUMN = { "TC", "Ad-Soyad", "Maaş", "Görev", "İşçilik Ücreti","Açıklama"};
    
    private String tc;
    private String ad;
    private String soyad;
    private String adres;
    private String tel;
    private String gorev;
    private String bas_tarih;
    private float maas;

    public Personel(String tc, String ad, String sad, String adr, String tel, String g, String bt, float m) {
        this.tc = tc;
        this.ad = ad;
        this.soyad = sad;
        this.adres = adr;
        this.tel = tel;
        this.gorev = g;
        this.bas_tarih = bt;
        this.maas = m;
    }

    public static void createPersonelTable() {
        String sorgu = "CREATE TABLE IF NOT EXISTS Personel("
                + "tc TEXT PRIMARY KEY NOT NULL," + "ad TEXT NOT NULL,"
                + "soyad TEXT NOT NULL," + "adres TEXT NOT NULL,"
                + "tel TEXT NOT NULL," + "gorev TEXT NOT NULL,"
                + "maas REAL NOT NULL," + "bas_tarih TEXT NOT NULL)";
        try {
            Statement stmt = Database.getConnection().createStatement();
            stmt.executeUpdate(sorgu);
            stmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Personel tablosu oluşturulamadı!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
    }

    public boolean insertPersonel() {
        int sonuc = 0;
        String sorgu = "INSERT INTO Personel VALUES(?,?,?,?,?,?,?,?);";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setString(1, this.tc);
            psmt.setString(2, this.ad);
            psmt.setString(3, this.soyad);
            psmt.setString(4, this.adres);
            psmt.setString(5, this.tel);
            psmt.setString(6, this.gorev);
            psmt.setFloat(7, this.maas);
            psmt.setString(8, this.bas_tarih);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Personel ekleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public boolean updatePersonel() {
        int sonuc = 0;
        String sorgu = "UPDATE Personel SET ad=?, soyad=?, adres=?, tel=?, gorev=?, maas=?, bas_tarih=? WHERE tc=?";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setString(1, this.ad);
            psmt.setString(2, this.soyad);
            psmt.setString(3, this.adres);
            psmt.setString(4, this.tel);
            psmt.setString(5, this.gorev);
            psmt.setFloat(6, this.maas);
            psmt.setString(7, this.bas_tarih);
            psmt.setString(8, this.tc);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Personel güncelleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static boolean deletePersonel(String tc) {
        int sonuc = 0;
        String sorgu = "DELETE FROM Personel WHERE tc=?";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setString(1, tc);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Personel silme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static String[] getPersonel(String id)
    {
        String[] personel = null;
        String sorgu = "SELECT * FROM Personel WHERE tc=?";
        try {
            PreparedStatement pstmt = Database.getConnection().prepareStatement(sorgu);
            pstmt.setString(1, id);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                personel = new String[] {
                    rs.getString("ad"),
                    rs.getString("soyad"),
                    String.valueOf(rs.getFloat("maas")),
                    rs.getString("gorev"),
                    rs.getString("tel"),
                    rs.getString("bas_tarih"),
                    rs.getString("adres")
                };
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Personel elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return personel;           
    }
    
    public static Object[][] getAllPersonel(Object o) {
        Object[][] personelList = null;
        String sorgu = "FROM Personel WHERE ad LIKE '" + o + "%' OR tc LIKE '" + o + "%';";
        
        try {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount " + sorgu);

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                personelList = new Object[rowIndex][PERSONEL_COLUMN.length];
                rs = pstmt.executeQuery("SELECT * " + sorgu);
                rowIndex = 0;

                while (rs.next()) {
                    personelList[rowIndex][0] = rs.getString("tc");
                    personelList[rowIndex][1] = (rs.getString("ad") +" "+ rs.getString("soyad"));
                    personelList[rowIndex][2] = rs.getString("adres");
                    personelList[rowIndex][3] = rs.getString("tel");
                    personelList[rowIndex][4] = rs.getString("gorev");
                    personelList[rowIndex][5] = rs.getString("bas_tarih");
                    personelList[rowIndex][6] = rs.getFloat("maas");
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tüm personeli elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return personelList;
    }
    
    public static Object[][] getAllPersonelOfFatura(int fid) {
        Object[][] fpList = null;
        String sorgu = "From Tesisat INNER JOIN Personel ON Tesisat.personel_id = Personel.tc Where fatura_id=" + fid+"; ";
        try {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount " + sorgu);

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                fpList = new Object[rowIndex][FATURALIPERSONEL_COLUMN.length];
                rs = pstmt.executeQuery("SELECT ad, soyad, ucret,aciklama " + sorgu);
                rowIndex = 0;

                while (rs.next()) {
                    fpList[rowIndex][0] = rs.getString("ad") + rs.getString("soyad");
                    fpList[rowIndex][1] = rs.getFloat("ucret");
                    fpList[rowIndex][2] = rs.getString("aciklama");
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Faturalı tüm personeli elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return fpList;
    }

    public static float[] information() {
        float[] f = new float[2];
        String sorgu = "Select count(*) as A, sum(maas) as B from Personel;";
        try {
            PreparedStatement pstmt = Database.getConnection().prepareStatement(sorgu);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                f[0] = rs.getFloat("A");
                f[1] = rs.getFloat("B");
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Personel sayısı elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return f;
    }
    
    public static Object[][] getAllPersonelForSiparis() {
        Object[][] personelList = null;
        
        try {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount FROM Personel;");

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                personelList = new Object[rowIndex][SIPARISPERSONEL_COLUMN.length];
                rs = pstmt.executeQuery("SELECT tc, ad, soyad, maas, gorev FROM Personel;" );
                rowIndex = 0;

                while (rs.next()) {
                    personelList[rowIndex][0] = rs.getString("tc");
                    personelList[rowIndex][1] = (rs.getString("ad") +" "+ rs.getString("soyad"));
                    personelList[rowIndex][2] = rs.getFloat("maas");
                    personelList[rowIndex][3] = rs.getString("gorev");
                    personelList[rowIndex][5] = "";
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tüm personeli sipariş için elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return personelList;
    }
}
