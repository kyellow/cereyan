/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author kyellow
 */
public class Tesisat {

    private int fatura_id;
    private String personel_id;
    private float ucret;
    private String aciklama;

    public Tesisat(int fid, String pid, float u, String a) {
        this.fatura_id = fid;
        this.personel_id = pid;
        this.ucret = u;
        this.aciklama = a;
    }

    public static void createTesisatTable() {
        String sorgu = "CREATE TABLE IF NOT EXISTS Tesisat("
                + "fatura_id INT NOT NULL," + "personel_id TEXT NOT NULL,"
                + "ucret REAL NOT NULL," + "aciklama TEXT NOT NULL, PRIMARY KEY(fatura_id,personel_id) )";
        try {
            Statement stmt = Database.getConnection().createStatement();
            stmt.executeUpdate(sorgu);
            stmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Ürün tablosu oluşturulamadı!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
    }

    public boolean insertTesisat() {
        int sonuc = 0;
        String sorgu = "INSERT INTO Tesisat VALUES(?,?,?,?);";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.fatura_id);
            psmt.setString(2, this.personel_id);
            psmt.setFloat(3, this.ucret);
            psmt.setString(4, this.aciklama);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tesisat ekleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static void insertTesisatList(ArrayList<Tesisat> tlist) throws SQLException {
        if(tlist == null || tlist.isEmpty())
            return;
        
        String sorgu = "INSERT INTO Tesisat VALUES(?,?,?,?);";

        try {
            Database.getConnection().setAutoCommit(false);
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            for (int i = 0; i < tlist.size(); i++) {
                psmt.setInt(1, tlist.get(i).fatura_id);
                psmt.setString(2, tlist.get(i).personel_id);
                psmt.setFloat(3, tlist.get(i).ucret);
                psmt.setString(4, tlist.get(i).aciklama);
                psmt.executeUpdate();
            }
            Database.getConnection().commit();
            Database.getConnection().setAutoCommit(true);
            psmt.close();
        } catch (SQLException ex) {
            Database.getConnection().rollback();
            Tools.logMsg("Tesisat listesini ekleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
            throw ex;
        }
    }

    public boolean updateTesisat() {
        int sonuc = 0;
        String sorgu = "UPDATE Tesisat SET personel_id=?, ucret=?, aciklama=? WHERE fatura_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setString(1, this.personel_id);
            psmt.setFloat(2, this.ucret);
            psmt.setString(3, this.aciklama);
            psmt.setInt(4, this.fatura_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tesisat güncelleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public boolean deleteTesisat() {
        int sonuc = 0;
        String sorgu = "DELETE FROM Tesisat WHERE fatura_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.fatura_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tesisat silme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }
}
