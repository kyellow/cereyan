/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author kyellow
 */
public class Musteri {

    public static final Object[] MUSTERI_COLUMN = {"Müşteri No", "Ad-Soyad", "Adres","İş Tel", "Cep Tel", "Fax","E-mail"};
    
    private int musteri_id;
    private boolean tip;
    private String tc;
    private String ad_soyad;
    private String is_tel;
    private String cep_tel;
    private String email;
    private String adres;
    private String fax;

    public Musteri(int id, boolean tip, String tc, String as, String it, String ct, String e, String a, String f) {
        this.musteri_id = id;
        this.tip = tip;
        this.tc = tc;
        this.ad_soyad = as;
        this.is_tel = it;
        this.cep_tel = ct;
        this.email = e;
        this.adres = a;
        this.fax = f;
    }

    public static void createMusteriTable() {
        String sorgu = "CREATE TABLE IF NOT EXISTS Musteri("
                + "musteri_id INT PRIMARY KEY NOT NULL," + "tip BOOLEAN NOT NULL,"
                + "tc TEXT NOT NULL," + "ad_soyad TEXT NOT NULL,"
                + "is_tel TEXT NOT NULL," + "cep_tel TEXT NOT NULL,"
                + "email TEXT NOT NULL," + "adres TEXT NOT NULL,"
                + "fax TEXT NOT NULL)";
        try {
            Statement stmt = Database.getConnection().createStatement();
            stmt.executeUpdate(sorgu);
            stmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Müşteri tablosu oluşturma hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
    }

    public boolean insertMusteri() {
        int sonuc = 0;
        String sorgu = "INSERT INTO Musteri VALUES(?,?,?,?,?,?,?,?,?);";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.musteri_id);
            psmt.setBoolean(2, this.tip);
            psmt.setString(3, this.tc);
            psmt.setString(4, this.ad_soyad);
            psmt.setString(5, this.is_tel);
            psmt.setString(6, this.cep_tel);
            psmt.setString(7, this.email);
            psmt.setString(8, this.adres);
            psmt.setString(9, this.fax);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Müşteri ekleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public boolean updateMusteri() {
        int sonuc = 0;
        String sorgu = "UPDATE Musteri SET tip=?, tc=?, ad_soyad=?, is_tel=?, cep_tel=?, email=?, adres=?, fax=? WHERE musteri_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setBoolean(1, this.tip);
            psmt.setString(2, this.tc);
            psmt.setString(3, this.ad_soyad);
            psmt.setString(4, this.is_tel);
            psmt.setString(5, this.cep_tel);
            psmt.setString(6, this.email);
            psmt.setString(7, this.adres);
            psmt.setString(8, this.fax);
            psmt.setInt(9, this.musteri_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Müşteri güncelleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static boolean deleteMusteri(int mId) {
        int sonuc = 0;
        String sorgu = "DELETE FROM Musteri WHERE musteri_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, mId);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Müşteri silme hatası! Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }
    
    
    public static String[] getMusteri(int mid)
    {
        String[] musteri = null;
        String sorgu =  "SELECT * FROM Musteri WHERE musteri_id=?;";
        try {
            PreparedStatement pstmt = Database.getConnection().prepareStatement(sorgu);
            pstmt.setInt(1, mid);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                musteri = new String[] {
                    String.valueOf(rs.getInt("musteri_id")),
                    String.valueOf(rs.getBoolean("tip")),
                    rs.getString("tc"),
                    rs.getString("ad_soyad"),
                    rs.getString("is_tel"),
                    rs.getString("cep_tel"),
                    rs.getString("email"),
                    rs.getString("adres"),
                    rs.getString("fax")
                };
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Müşteri elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return musteri;
    }

    public static Object[][] getAllMusteri(Object o, int a) {
        Object[][] musteriList = null;
        String sorgu;
        switch (a) {
            case 0:     // firma
                sorgu = "FROM Musteri WHERE tip=0 AND (musteri_id LIKE '" + o + "%' OR ad_soyad LIKE '" + o + "%');";
                break;
            case 1:     // kisi
                sorgu = "FROM Musteri WHERE tip=1 AND (musteri_id LIKE '" + o + "%' OR ad_soyad LIKE '" + o + "%');";
                break;
            default:
                sorgu = "FROM Musteri";
                break;
        }

        try {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount " + sorgu);

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                musteriList = new Object[rowIndex][MUSTERI_COLUMN.length];
                rs = pstmt.executeQuery("SELECT * " + sorgu);
                rowIndex = 0;

                while (rs.next()) {
                    musteriList[rowIndex][0] = rs.getInt("musteri_id");
                    musteriList[rowIndex][1] = rs.getString("ad_soyad");
                    musteriList[rowIndex][2] = rs.getString("adres");
                    musteriList[rowIndex][3] = rs.getString("is_tel");
                    musteriList[rowIndex][4] = rs.getString("cep_tel");
                    musteriList[rowIndex][5] = rs.getString("email");
                    musteriList[rowIndex][6] = rs.getString("fax");
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tüm müşterileri  elde etme hatası! Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return musteriList;
    }

    public static String[] getAllMusteriName()
    {
        String[] musteriList = null;
        try {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount FROM Musteri");

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                musteriList = new String[rowIndex];
                rs = pstmt.executeQuery("SELECT musteri_id, ad_soyad FROM Musteri");
                rowIndex = 0;

                while (rs.next()) {
                    musteriList[rowIndex++] = String.valueOf(rs.getInt("musteri_id")) + "-" + rs.getString("ad_soyad");
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tüm müşteri isimleri elde etme hatası! Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return musteriList;
    }
    
    public static int[] information() {
        int[] f = new int[3];
        String sorgu1 = "Select count(*) as A from Musteri;";
        String sorgu2 = "Select count(*) as B from Musteri where tip=0;";
        String sorgu3 = "Select count(*) as C from Musteri where tip=1;";
        try {
            PreparedStatement pstmt = Database.getConnection().prepareStatement(sorgu1);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                f[0] = rs.getInt("A");
            }
            pstmt = Database.getConnection().prepareStatement(sorgu2);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                f[1] = rs.getInt("B");
            }
            pstmt = Database.getConnection().prepareStatement(sorgu3);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                f[2] = rs.getInt("C");
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Müşteri sayısını elde etme hatası! Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return f;
    }
}
