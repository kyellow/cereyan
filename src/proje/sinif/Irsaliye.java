/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author kyellow
 */
public class Irsaliye {

    public static final Object[] IRSALIYE_COLUMN = {"Fatura No","Müşteri Ad-Soyad", "Tarih", "Toplam Tutar"};

    private int fatura_id;
    private String ad_soyad;
    private Date tarih;
    private float tutar;

    public Irsaliye(int fid, String as, Date t, float tt) {
        this.fatura_id = fid;
        this.ad_soyad = as;
        this.tarih = t;
        this.tutar = tt;
    }

    public static Object[][] getAllIrsaliye(Object o) {
        String sorgu;
        Object[][] irsaliyeList = null;
        
        if (o instanceof String) {
            sorgu = "FROM Siparis AS S INNER JOIN  Musteri AS M USING(musteri_id) WHERE M.ad_soyad LIKE '" + o + "%';";
        } else {
            sorgu = "FROM Siparis AS S INNER JOIN Musteri AS M USING(musteri_id) WHERE S.fatura_id LIKE '" + o + "%';";
        }

        try {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount " + sorgu);

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                irsaliyeList = new Object[rowIndex][IRSALIYE_COLUMN.length];
                rs = pstmt.executeQuery("SELECT fatura_id,ad_soyad,tarih,tutar " + sorgu);
                rowIndex = 0;

                while (rs.next()) {
                    irsaliyeList[rowIndex][0] = rs.getInt("fatura_id");
                    irsaliyeList[rowIndex][1] = rs.getString("ad_soyad");
                    irsaliyeList[rowIndex][2] = rs.getDate("tarih");
                    irsaliyeList[rowIndex][3] = rs.getFloat("tutar");
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tüm irsaliyeleri elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return irsaliyeList;
    }
}
