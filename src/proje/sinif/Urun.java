/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author kyellow
 */
public class Urun {

    public static final Object[] URUN_COLUMN = {"Ürün No", "Ürün Adı", "Ürün Sayısı", "KDV Oranı", "Alış Fiyatı", "Satış Fiyatı"};
    public static final Object[] FATURALIURUN_COLUMN = {"Ürün Adı", "Adet"};
    public static final Object[] SIPARISURUN_COLUMN = {"Ürün No", "Adı", "Mevcut","Ücret", "Adet"};

    private int urun_id;
    private String ad;
    private int depo_adet;
    private int min_adet;
    private float kdv;
    private float alis_fiyat;
    private float satis_fiyat;
    private int firma_id;

    public Urun(int id, String ad, int da, int ma, float k, float af, float sa, int fid) {
        this.urun_id = id;
        this.ad = ad;
        this.depo_adet = da;
        this.min_adet = ma;
        this.kdv = k;
        this.alis_fiyat = af;
        this.satis_fiyat = sa;
        this.firma_id = fid;
    }

    public static void createUrunTable(){
        String sorgu = "CREATE TABLE IF NOT EXISTS Urun("
                + "urun_id INT PRIMARY KEY NOT NULL," + "ad TEXT NOT NULL,"
                + "depo_adet INT NOT NULL," + "min_adet INT NOT NULL,"
                + "kdv REAL NOT NULL," + "alis_fiyat REAL NOT NULL,"
                + "satis_fiyat REAL NOT NULL," + "firma_id INT NOT NULL,"
                + "FOREIGN KEY (firma_id) REFERENCES Firma(firma_id))";
        try
        {
            Statement stmt = Database.getConnection().createStatement();
            stmt.executeUpdate(sorgu);
            stmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Ürün tablosu oluşturulamadı!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
    }

    public boolean insertUrun() {
        int sonuc = 0;
        String sorgu = "INSERT INTO Urun VALUES(?,?,?,?,?,?,?,?);";
        try
        {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.urun_id);
            psmt.setString(2, this.ad);
            psmt.setInt(3, this.depo_adet);
            psmt.setInt(4, this.min_adet);
            psmt.setFloat(5, this.kdv);
            psmt.setFloat(6, this.alis_fiyat);
            psmt.setFloat(7, this.satis_fiyat);
            psmt.setInt(8, this.firma_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        }
        catch(SQLException ex)
        {
            Tools.logMsg("Ürün ekleme hatası!\n Açıklama :"
                    + ex.getMessage(),  Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public boolean updateUrun() {
        int sonuc = 0;
        String sorgu = "UPDATE Urun SET ad=?, depo_adet=?, min_adet=?, kdv=?, alis_fiyat=?, satis_fiyat=?,firma_id=? WHERE urun_id=?;";
        try
        {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setString(1, this.ad);
            psmt.setInt(2, this.depo_adet);
            psmt.setInt(3, this.min_adet);
            psmt.setFloat(4, this.kdv);
            psmt.setFloat(5, this.alis_fiyat);
            psmt.setFloat(6, this.satis_fiyat);
            psmt.setInt(7, this.firma_id);
            psmt.setInt(8, this.urun_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        }
        catch(SQLException ex)
        {
            Tools.logMsg("Ürün güncelleme hatası!\n Açıklama :"
                    + ex.getMessage(),  Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static void decreaseStock(ArrayList<Integer> frans) throws SQLException {
        String sorgu = "UPDATE Urun SET  depo_adet=? WHERE urun_id=?;";

        try {
            Database.getConnection().setAutoCommit(false);
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            for (int i = 0; i < frans.size(); i += 2) {
                if (frans.get(i + 1) == 0) {
                    break;
                }
                psmt.setInt(1, frans.get(i));
                psmt.setInt(2, frans.get(i + 1));
                psmt.executeUpdate();
            }
            Database.getConnection().commit();
            Database.getConnection().setAutoCommit(true);
            psmt.close();
        } catch (SQLException ex) {
            Database.getConnection().rollback();
            Tools.logMsg("Staoktan ürün azaltma hatası!\n Açıklama :"
                    + ex.getMessage(),  Tools.MessageType.ERROR);
            throw ex;
        }
    }

    public static boolean deleteUrun(int uId){
        int sonuc = 0;
        String sorgu = "DELETE FROM Urun WHERE urun_id=?;";
        try
        {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, uId);
            sonuc = psmt.executeUpdate();
            psmt.close();
        }
        catch(SQLException ex)
        {
            Tools.logMsg("Ürün silme hatası!\n Açıklama :"
                    + ex.getMessage(),  Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }
    
    public static String[] getUrun(int uid)
    {
        String[] urun = null;
        String sorgu = "SELECT * FROM Urun WHERE urun_id=?";
        try {
            PreparedStatement pstmt = Database.getConnection().prepareStatement(sorgu);
            pstmt.setInt(1, uid);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                urun = new String[] {
                    String.valueOf(rs.getInt("urun_id")),
                    rs.getString("ad"),
                    String.valueOf(rs.getInt("depo_adet")),
                    String.valueOf(rs.getInt("min_adet")),
                    String.valueOf(rs.getFloat("kdv")),
                    String.valueOf(rs.getFloat("alis_fiyat")),
                    String.valueOf(rs.getFloat("satis_fiyat")),
                    String.valueOf(rs.getInt("firma_id"))
                };
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Urun elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return urun;           
    }

    public static Object[][] getAllUrunOfFatura(int fatura_id) {
        Object[][] urunList = null;
        String sorgu = "From Urun Where urun_id IN (Select urun_id From Irsaliye Where fatura_id=" + fatura_id + ");";
        try
        {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount " + sorgu);

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                urunList = new Object[rowIndex][FATURALIURUN_COLUMN.length];
                rs = pstmt.executeQuery("SELECT * " + sorgu);
                rowIndex = 0;
                
                while (rs.next()) {
                    urunList[rowIndex][0] = rs.getString("ad");
                    urunList[rowIndex][1] = rs.getInt("depo_adet");
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        }catch(SQLException ex)
        {
            Tools.logMsg("Ürün elde etme hatası!\n Açıklama :"
                    + ex.getMessage(),  Tools.MessageType.ERROR);
        }
        return urunList;
    }

    public static Object[][] getAllUrun(Object o) {
        String sorgu;
        Object[][] urunList = null;
        if (o instanceof String) {
            sorgu = "FROM Urun WHERE ad LIKE '" + o + "%';";
        } else {
            sorgu = "FROM Urun WHERE urun_id LIKE '" + o + "%';";
        }
        
        try
        {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount " + sorgu);

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                urunList = new Object[rowIndex][URUN_COLUMN.length];
                rs = pstmt.executeQuery("SELECT * " + sorgu);
                rowIndex = 0;
                
                while (rs.next()) {
                    urunList[rowIndex][0] = rs.getInt("urun_id");
                    urunList[rowIndex][1] = rs.getString("ad");
                    urunList[rowIndex][2] = rs.getInt("depo_adet");
                    urunList[rowIndex][3] = rs.getFloat("kdv");
                    urunList[rowIndex][4] = rs.getFloat("alis_fiyat");
                    urunList[rowIndex][5] = rs.getFloat("satis_fiyat");
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        }catch(SQLException ex)
        {
            Tools.logMsg("Tüm ürünleri elde etme hatası!\n Açıklama :"
                    + ex.getMessage(),  Tools.MessageType.ERROR);
        }
        return urunList;
    }

    public static int[] getStockInfo(){
        int[] f = new int[3];
        String sorgu1 = "Select count(*) as A FROM Urun;";
        String sorgu2 = "Select count(*) as B FROM Urun where depo_adet<=min_adet;";
        String sorgu3 = "select SUM(adet) as C from Irsaliye;";
        try
        {
            PreparedStatement pstmt = Database.getConnection().prepareStatement(sorgu1);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                f[0] = rs.getInt("A");
            }
            pstmt = Database.getConnection().prepareStatement(sorgu2);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                f[1] = rs.getInt("B");
            }
            pstmt = Database.getConnection().prepareStatement(sorgu3);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                f[2] = rs.getInt("C");
            }
            pstmt.close();
            rs.close();
        }
        catch(SQLException ex)
        {
            Tools.logMsg("Stok bilgisini elde etme hatası!\n Açıklama :"
                    + ex.getMessage(),  Tools.MessageType.ERROR);
        }
        return f;
    }
    
    public static Object[][] getAllUrunForSiparis() {
        Object[][] urunList = null;
        
        try
        {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) AS rowcount FROM Urun WHERE depo_adet > 0;");

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                urunList = new Object[rowIndex][SIPARISURUN_COLUMN.length];
                rs = pstmt.executeQuery("SELECT  urun_id, ad, depo_adet, satis_fiyat FROM Urun WHERE depo_adet > 0;");
                rowIndex = 0;
                
                while (rs.next()) {
                    urunList[rowIndex][0] = rs.getInt("urun_id");
                    urunList[rowIndex][1] = rs.getString("ad");
                    urunList[rowIndex][2] = rs.getInt("depo_adet");
                    urunList[rowIndex][3] = rs.getFloat("satis_fiyat");
                    rowIndex++;
                }
            }
            pstmt.close();
            rs.close();
        }catch(SQLException ex)
        {
            Tools.logMsg("Tüm ürünleri elde etme hatası!\n Açıklama :"
                    + ex.getMessage(),  Tools.MessageType.ERROR);
        }
        return urunList;
    }
}
