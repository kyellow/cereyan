/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author kyellow
 */
public class Firma {

    private int firma_id;
    private String firma_ad;
    private String adres;
    private String tel;
    private String fax;
    private String email;

    public Firma(int f, String ad, String adres, String tel, String fax, String email) {
        this.firma_id = f;
        this.firma_ad = ad;
        this.adres = adres;
        this.tel = tel;
        this.fax = fax;
        this.email = email;
    }

    public static void createFirmaTable() {
        String sorgu = "CREATE TABLE IF NOT EXISTS Firma("
                + "firma_id INT PRIMARY KEY NOT NULL," + "firma_ad TEXT NOT NULL,"
                + "adres TEXT NOT NULL," + "tel TEXT NOT NULL,"
                + "fax TEXT NOT NULL," + "email TEXT NOT NULL)";
        try {
            Statement stmt = Database.getConnection().createStatement();
            stmt.executeUpdate(sorgu);
            stmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Firma tablosu oluşturulamadı!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
    }

    public boolean insertFirma() {
        int sonuc = 0;
        String sorgu = "INSERT INTO Firma VALUES(?,?,?,?,?,?);";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.firma_id);
            psmt.setString(2, this.firma_ad);
            psmt.setString(3, this.adres);
            psmt.setString(4, this.tel);
            psmt.setString(5, this.fax);
            psmt.setString(6, this.email);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Firma ekleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public boolean updateFirma() {
        int sonuc = 0;
        String sorgu = "UPDATE Firma SET firma_ad=?, adres=?, tel=?, fax=?, email=? WHERE firma_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setString(1, this.firma_ad);
            psmt.setString(2, this.adres);
            psmt.setString(3, this.tel);
            psmt.setString(4, this.fax);
            psmt.setString(5, this.email);
            psmt.setInt(6, this.firma_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Firma güncelleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public boolean deleteFirma() {
        int sonuc = 0;
        String sorgu = "DELETE FROM Firma WHERE firma_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.firma_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Firma silme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }
   
    public static String[] getAllFirmaName(){
        String[] firmaList = null;
        try {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount FROM Firma ");

            if(rs.next())
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                firmaList = new String[rowIndex];
                rs = pstmt.executeQuery("SELECT firma_id, firma_ad FROM Firma");
                rowIndex = 0;

                while (rs.next()) {
                    firmaList[rowIndex++] = String.valueOf(rs.getInt("firma_id")) + "-" + rs.getString("firma_ad");
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Tüm firma isimleri elde etme hatası! Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return firmaList;   
    }

    public static int[] getAllFirmaID() {
        int[] fIDs = null;
        try {
            Statement pstmt = Database.getConnection().createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT COUNT(*) as rowcount FROM Firma;");

            if (rs.next()) 
            {
                int rowIndex = rs.getInt("rowcount");
                rs.close();
                
                fIDs = new int[rowIndex];
                rs = pstmt.executeQuery("SELECT firma_id FROM Firma;");
                rowIndex = 0;

                while (rs.next()) {
                    fIDs[rowIndex++] = rs.getInt("firma_id");
                }
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Firma kimliklerini elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return fIDs;
    }

    public static String[] getFirma(int id, boolean byUrunId) {
        String[] frm = null;
        String sorgu;
        if (byUrunId) {
            sorgu = "SELECT * FROM Firma as F INNER JOIN Urun as U USING(firma_id) WHERE U.urun_id=?;";
        } else {
            sorgu = "SELECT * FROM Firma WHERE firma_id=?;";
        }

        try {
            PreparedStatement pstmt = Database.getConnection().prepareStatement(sorgu);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                frm = new String[]{
                    String.valueOf(rs.getInt("firma_id")),
                    (rs.getString("firma_ad")),
                    (rs.getString("adres")),
                    (rs.getString("tel")),
                    (rs.getString("fax")),
                    (rs.getString("email"))
                };
            }
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            Tools.logMsg("Firma elde etme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return frm;
    }
}
