/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.io.File;
import java.sql.Connection;         
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author kyellow
 */
public class Database {

    private static Database mInstance = null;

    private static final String DATABASE_NAME = "cereyan.sqlite";
    private static final String[] TABLE_NAMES = {"Irsaliye", "Firma", "Musteri", "Personel", "Siparis", "Tesisat", "Urun"};

    private Connection con;    //Bağlantı parametrelerini tutar.

    public static Connection getConnection() throws SQLException {
        if (mInstance == null) {
            mInstance = new Database();
        }

        if (mInstance.con.isClosed()) {
            mInstance.connect();
        }

        return mInstance.con;
    }

    public static Database getDatabase() {
        if (mInstance == null) {
            mInstance = new Database();
        }

        return mInstance;
    }

    private Database() {
        this.connect();
    }

    private void connect() {

        try {
            //eğer bağlantı varsa true gönder
            if (con != null) {
                if (con.isClosed() == false) {
                    return;
                }
            }

            File db = new File(Database.DATABASE_NAME);
            boolean exist = db.exists();        // ilk defa mı oluşturuluyor?

            /*  // MySQL Connection
                Class.forName("com.mysql.jdbc.Driver");     // mysql JDBC driver
                con = DriverManager.getConnection("jdbc:mysql://localhost/cereyan",Database.user,Database.password);
            */
            // Sqlite Connection
            Class.forName("org.sqlite.JDBC");  //Bağlantıyı oluşturabilmek için kullanılan sürücünün bulunduğu sınıfın olduğu paket
            con = (Connection) DriverManager.getConnection("jdbc:sqlite:" + Database.DATABASE_NAME);

            if (exist == false) {
                this.createTables();
            }

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    private void createTables() throws Exception {
        Musteri.createMusteriTable();
        Firma.createFirmaTable();
        Personel.createPersonelTable();
        Urun.createUrunTable();
        Fatura.createFaturaTable();
        Siparis.createSiparisTable();
        Tesisat.createTesisatTable();
    }

    @Override
    public void finalize() throws SQLException {
        this.disconnect();
    }

    public void disconnect() throws SQLException {
        if (con != null) {
            if (con.isClosed()) {
                con.close();
            }
        }
    }
    /*
    public void clearAllTables() throws SQLException {
        for (String s : Database.TABLE_NAMES) {
            clearTable(s);
        }
    }

    public void clearTable(String tabloAdi) throws SQLException {
        String sorgu = "DELETE FROM " + tabloAdi;
        Statement stmt = con.createStatement();
        stmt.executeUpdate(sorgu);
        stmt.close();
    }

    public String[] getTableColumnNames(String tabloAdi) throws SQLException {
        String sorgu = "SELECT * FROM " + tabloAdi;
        PreparedStatement psmt = con.prepareStatement(sorgu);
        ResultSetMetaData metaData = psmt.getMetaData();

        int count = metaData.getColumnCount();
        String[] sonuc = new String[count];
        for (int i = 0; i < count; i++) {
            sonuc[i] = metaData.getColumnName(i + 1);
        }
        psmt.close();
        return sonuc;
    }
    */
}
