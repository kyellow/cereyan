/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author kyellow
 */
public class Siparis {

    private int fatura_id;
    private int musteri_id;
    private String tarih;
    private float tutar;
    private float kalan;

    public Siparis(int fid, int mid, String date, float t, float k) {
        this.fatura_id = fid;
        this.musteri_id = mid;
        this.tarih = date;
        this.tutar = t;
        this.kalan = k;
    }

    public static void createSiparisTable() {
        String sorgu = "CREATE TABLE IF NOT EXISTS Siparis("
                + "fatura_id INT PRIMARY KEY NOT NULL," + "musteri_id INT NOT NULL,"
                + "tarih TEXT NOT NULL," + "tutar REAL NOT NULL,"
                + "kalan REAL NOT NULL,"
                + "FOREIGN KEY (musteri_id) REFERENCES Musteri(musteri_id))";
        try {
            Statement stmt = Database.getConnection().createStatement();
            stmt.executeUpdate(sorgu);
            stmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Sipariş tablosu oluşturma hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
    }

    public boolean insertSiparis() {
        int sonuc = 0;
        String sorgu = "INSERT INTO Siparis VALUES(?,?,?,?,?);";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.fatura_id);
            psmt.setInt(2, this.musteri_id);
            psmt.setString(3, this.tarih);
            psmt.setFloat(4, this.tutar);
            psmt.setFloat(5, this.kalan);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Sipariş ekleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public boolean updateSiparis() {
        int sonuc = 0;
        String sorgu = "UPDATE Siparis SET musteri_id=?, tarih=?, tutar=?, kalan=? WHERE fatura_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.musteri_id);
            psmt.setString(2, this.tarih);
            psmt.setFloat(3, this.tutar);
            psmt.setFloat(4, this.kalan);
            psmt.setInt(5, this.fatura_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Sipariş güncelleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static boolean deleteSiparis(int fid) {
        int sonuc = 0;
        String sorgu = "DELETE FROM Siparis WHERE fatura_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, fid);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Sipariş silme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static boolean makePayment(float kalan, int fatura_id) {
        int sonuc = 0;
        String sorgu = "UPDATE Siparis SET kalan=? WHERE fatura_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setFloat(1, kalan);
            psmt.setInt(2, fatura_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Ödeme yapma hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }
}
