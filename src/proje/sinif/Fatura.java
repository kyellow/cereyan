/*
 * Copyright (C) 2012 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package proje.sinif;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author kyellow
 */
public class Fatura {

    private int fatura_id;
    private int urun_id;
    private int adet;

    public Fatura(int f, int u, int a) {
        this.fatura_id = f;
        this.urun_id = u;
        this.adet = a;
    }

    public static void createFaturaTable() {
        String sorgu = "CREATE TABLE IF NOT EXISTS Irsaliye("
                + "fatura_id INT NOT NULL," + "urun_id INT NOT NULL,"
                + "adet INT NOT NULL,"
                + "PRIMARY KEY (fatura_id,urun_id) )";
        try {
            Statement stmt = Database.getConnection().createStatement();
            stmt.executeUpdate(sorgu);
            stmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Fatura tablosu oluşturulamadı!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
    }

    public boolean insertFatura() {
        int sonuc = 0;
        String sorgu = "INSERT INTO Irsaliye VALUES(?,?,?);";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.fatura_id);
            psmt.setInt(2, this.urun_id);
            psmt.setInt(3, this.adet);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Fatura ekle hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static void insertFaturaList(ArrayList<Fatura> flist) throws SQLException {
        if(flist == null || flist.isEmpty())
            return;
        
        String sorgu = "INSERT INTO Irsaliye VALUES(?,?,?);";

        try {
            Database.getConnection().setAutoCommit(false);
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            for (int i = 0; i < flist.size(); i++) {
                psmt.setInt(1, flist.get(i).fatura_id);
                psmt.setInt(2, flist.get(i).urun_id);
                psmt.setInt(3, flist.get(i).adet);
                psmt.executeUpdate();
            }
            Database.getConnection().commit();
            Database.getConnection().setAutoCommit(true);
            psmt.close();
        } catch (SQLException ex) {
            Database.getConnection().rollback();
            Tools.logMsg("Fatura listesi ekle hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
            throw ex;
        }
    }

    public boolean updateFatura() {
        int sonuc = 0;
        String sorgu = "UPDATE Irsaliye SET urun_id=?, Adet=? WHERE fatura_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, this.urun_id);
            psmt.setInt(2, this.adet);
            psmt.setInt(3, this.fatura_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Fatura güncelleme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }

    public static boolean deleteFatura(int f_id) {
        int sonuc = 0;
        String sorgu = "DELETE FROM Irsaliye WHERE fatura_id=?;";
        try {
            PreparedStatement psmt = Database.getConnection().prepareStatement(sorgu);
            psmt.setInt(1, f_id);
            sonuc = psmt.executeUpdate();
            psmt.close();
        } catch (SQLException ex) {
            Tools.logMsg("Fatura silme hatası!\n Açıklama :"
                    + ex.getMessage(), Tools.MessageType.ERROR);
        }
        return (sonuc > 0);
    }
    
    

}
